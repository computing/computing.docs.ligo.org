# computing.docs.ligo.org

This repo defines the index for the <https://computing.docs.ligo.org>
subdomain.

It's only function is to immediately redirect all visitors to
the [IGWN Computing Guide](https://computing.docs.ligo.org/guide/) which
is managed at <https://git.ligo.org/computing/guide>.
